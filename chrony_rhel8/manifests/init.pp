class chrony_rhel8 {
	package { 'chrony': 
	  ensure => "present",
    	}
	service { 'chronyd':
  	  ensure => running,
	}
}
