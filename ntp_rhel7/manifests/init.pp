class ntp_rhel7 {
	package { 'ntp': 
	  ensure => "present",
    	}
	service { 'ntpd':
  	  ensure => running,
	}
}
