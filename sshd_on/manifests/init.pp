class sshd_on {
	$enhancers = [ 'openssh-server', 'openssh', 'openssh-clients' ]
	package { $enhancers: ensure => 'installed' } 
    	
	service { 'sshd': ensure => running }
}
