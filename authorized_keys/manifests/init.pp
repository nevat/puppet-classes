class authorized_keys {

ssh_authorized_key { 'david.test@host.com':
  ensure => present,
  user   => 'root',
  type   => 'ssh-rsa',
  key    => 'AAAAAADFDFDAFAFAFAFAFA',
}

ssh_authorized_key { 'daniel.test@host.com':
  ensure => present,
  user   => 'root',
  type   => 'ssh-rsa',
  key    => 'BBBBBDDDSDSDSDSDSDSDSD',
}

}
