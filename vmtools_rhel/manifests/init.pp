class vmtools_rhel {
	package { 'open-vm-tools': 
	  ensure => "present",
    	}
	service { 'vmtoolsd':
  	  ensure => running,
  	  enable => true,
	}
}
