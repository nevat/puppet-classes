# puppet-classes

Simple puppet classes.

Modules included:

- chrony_rhel8: Ensures chrony packages are installed and chronyd service is running. For EL8 systems.
- ntp_rhel7: Ensures ntp packages are installed and ntpd service is running. For EL7 systems.
- sshd_on: Ensures SSH packages are installed and sshd service is running. For EL7 and EL8 systems.
- vmtools_rhel: Ensures open-vm-tools package are installed and vmtools service is running. For EL7 and EL8 systems.
- authorized_keys: Ensures that specified SSH keys exist on the user authorized_keys file (root in the example).
